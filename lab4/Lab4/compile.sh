#!/bin/bash
# java compile for memorg management simulation

echo "start compile"

javac -cp ".:colt.jar:EvSchedSimul.jar" MemManageExp.java MemManage.java KernelFunctions.java

echo "end compile"
