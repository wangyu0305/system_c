import java.util.Random;
import java.util.concurrent.Semaphore;

public class Lab33
{
	// Configuration
        final static int PORT0 = 0;
	final static int PORT1 = 1;
	final static int MAXLOAD = 5;

	public static void main(String args[]) 
	{
		final int NUM_CARS = 10;
		int i;

		Ferry fer = new Ferry(PORT0,10);

		Auto [] automobile = new Auto[NUM_CARS];
		for (i=0; i< 7; i++) automobile[i] = new Auto(i,PORT0,fer);
		for ( ; i<NUM_CARS ; i++) automobile[i] = new Auto(i,PORT1,fer);

		Ambulance ambulance = new Ambulance(PORT0,fer);

			/* Start the threads */
 		fer.start();   // Start the ferry thread.
		for (i=0; i<NUM_CARS; i++) automobile[i].start();  // Start automobile threads
		ambulance.start();  // Start the ambulance thread.

		try {fer.join();} catch(InterruptedException e) { }; // Wait until ferry terminates.
		System.out.println("Ferry stopped.");
		// Stop other threads.
		for (i=0; i<NUM_CARS; i++) automobile[i].interrupt(); // Let's stop the auto threads.
		ambulance.interrupt(); // Stop the ambulance thread.
	}
}


class Auto extends Thread { // Class for the auto threads.

	private int id_auto;
	private int port;
	private Ferry fry;

	public Auto(int id, int prt, Ferry ferry)
	{
		this.id_auto = id;
		this.port = prt;
		this.fry = ferry;
	}

	public void run() 
        {

	   while (true) 
           {
		// Delay
		try {sleep((int) (3*Math.random()));} catch (Exception e) { break;}
		System.out.println("Auto " + id_auto + " arrives at port " + port);

		// Board
		fry.ambulance.acquireUninterruptibly();
		fry.ambulance.release();
		//System.out.println("Auto " + id_auto + "ready to enter");
		if(port == 0){
			fry.Port0.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.Port0.release();
				 break;
			}				
		}else{
			fry.Port1.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.Port1.release();
				 break;
			}
		}
		System.out.println("Auto " + id_auto + " boards on the ferry at port " + port);
		fry.addLoad();  // increment the ferry load
		
		if(fry.getLoad() == 5){
			fry.crossing.release();
		}else{
			if(port == 0){
				fry.Port0.release();		
			}else{
				fry.Port1.release();
			}
		}
 		
		// Arrive at the next port
		port = 1 - port ;   
		
		// disembark	
		fry.disembark.acquireUninterruptibly();	
		System.out.println("Auto " + id_auto + " disembarks from ferry at port " + port);
		fry.reduceLoad();   // Reduce load
		if(fry.getLoad() == 0 && !fry.getStopSign()){
			if(fry.getPort() == 0){
				fry.Port0.release();
			}else{
				fry.Port1.release();
			}
		}

		// Terminate
		if(isInterrupted()) break;
	   }
	   System.out.println("Auto "+id_auto+" terminated");
	}
 
}

class Ambulance extends Thread { // the Class for the Ambulance thread

	private int port;
	private Ferry fry;

	public Ambulance(int prt, Ferry ferry)
	{
		this.port = prt;
		this.fry = ferry;
	}

	public void run() 
        {
	   while (true) 
           {
		// Attente
		try {sleep((int) (1000*Math.random()));} catch (Exception e) { break;}
		System.out.println("Ambulance arrives at port " + port);

		// Board
		if(port == 0){
			fry.Port0.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.Port0.release();
				 break;
			}				
		}else{
			fry.Port1.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.Port1.release();
				 break;
			}
		}
		fry.ambulance.acquireUninterruptibly();
		System.out.println("Ambulance boards the ferry at port " + port);
		fry.addLoad();  // increment the load
		fry.crossing.release(); 
 		
		// Arrive at the next port
		port = 1 - port ;   

		//Disembarkment	
		fry.disembark.acquireUninterruptibly();	
		System.out.println("Ambulance disembarks the ferry at port " + port);
		fry.reduceLoad();   // Reduce load
		if(fry.getLoad() == 0){
			if(fry.getPort() == 0 && !fry.getStopSign()){
				fry.Port0.release();
			}else{
				fry.Port1.release();
			}
		}
		fry.ambulance.release();

		// Terminate
		if(isInterrupted()) break;
	   }
	   System.out.println("Ambulance terminates.");
	}
 
}

class Ferry extends Thread // The ferry Class
{
	private int port=0;  // Start at port 0
	private int load=0;  // Load is zero
	private int numCrossings;  // number of crossings to execute
	public Semaphore Port0;
	public Semaphore Port1;
	public Semaphore disembark;
	public Semaphore crossing;
	public Semaphore ambulance;
	private int loadAtOnce = 0;
	private boolean stop = false;
	// Semaphores

	public Ferry(int prt, int nbtours)
	{
		this.port = prt;
		numCrossings = nbtours;
		Port1 = new Semaphore(0);
		Port0 = new Semaphore(0);
		crossing = new Semaphore(0);
		disembark = new Semaphore(0);
		ambulance = new Semaphore(1);
		if(port == 0){
			Port0.release();
		}else{
			Port1.release();
		}
	}

	public void run() 
        {
	   int i;
	   System.out.println("Start at port " + port + " with a load of " + load + " vehicles");

	   // numCrossings crossings in our day
	   for(i=0 ; i < numCrossings ; i++)
           {
		// The crossing
		crossing.acquireUninterruptibly();
		System.out.println("Departure from port " + port + " with a load of " + load + " vehicles");
		System.out.println("Crossing " + i + " with a load of " + load + " vehicles");
		port = 1 - port;
		try {sleep((int) (100*Math.random()));} catch (Exception e) { }
		// Arrive at port
		System.out.println("Arrive at port " + port + " with a load of " + load + " vehicles");
		loadAtOnce = load;
		// Disembarkment et loading
		if(i == numCrossings-1) stop = true;
		for(int j = loadAtOnce; j > 0; j--){
			disembark.release();
		}
	   }
		System.out.println("release all resource");
	   	Port0.release();
		Port1.release();
		for(int j = loadAtOnce; j > 0; j--){
			disembark.release();
		}
	}

	// methodes to manipulate the load of the ferry
	public int getLoad()      { return(load); }
	public int getPort()	{return port;}
	public void addLoad()  { load = load + 1; }
	public void reduceLoad()  { load = load - 1 ;}
	public int getLoadAtOnce() { return loadAtOnce;}
	public boolean getStopSign() { return stop;}
}
