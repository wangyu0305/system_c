import java.util.Random;
import java.util.concurrent.Semaphore;

public class Lab3
{
	// Configuration
        final static int PORT0 = 0;
	final static int PORT1 = 1;
	final static int MAXLOAD = 5;

	public static void main(String args[]) 
	{
		final int NUM_CARS = 10;
		int i;

		Ferry fer = new Ferry(PORT0,10);

		Auto [] automobile = new Auto[NUM_CARS];
		for (i=0; i< 7; i++) automobile[i] = new Auto(i,PORT0,fer);
		for ( ; i<NUM_CARS ; i++) automobile[i] = new Auto(i,PORT1,fer);

		Ambulance ambulance = new Ambulance(PORT0,fer);

			/* Start the threads */
 		fer.start();   // Start the ferry thread.
		for (i=0; i<NUM_CARS; i++) automobile[i].start();  // Start automobile threads
		ambulance.start();  // Start the ambulance thread.

		try {fer.join();} catch(InterruptedException e) { }; // Wait until ferry terminates.
		System.out.println("Ferry stopped.");
		// Stop other threads.
		for (i=0; i<NUM_CARS; i++) automobile[i].interrupt(); // Let's stop the auto threads.
		ambulance.interrupt(); // Stop the ambulance thread.
	}
}


class Auto extends Thread { // Class for the auto threads.

	private int id_auto;
	private int port;
	private Ferry fry;

	public Auto(int id, int prt, Ferry ferry)
	{
		this.id_auto = id;
		this.port = prt;
		this.fry = ferry;
	}

	public void run() 
        {

	   while (true) 
           {
		// Delay
		try {sleep((int) (3*Math.random()));} catch (Exception e) { break;}
		System.out.println("Auto " + id_auto + " arrives at port " + port);

		// Board
		fry.ambulance.acquireUninterruptibly();
		fry.ambulance.release();
		if(port == 0){
			fry.carringPort0.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.carringPort0.release();
				 break;
			}				
		}else{
			fry.carringPort1.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.carringPort1.release();
				 break;
			}
		}
		System.out.println("Auto " + id_auto + " boards on the ferry at port " + port);
		fry.addLoad();  // increment the ferry load
		
		if(fry.getLoad() == 5){
			fry.crossing.release();
		}
 		
		// Arrive at the next port
		port = 1 - port ;   
		
		// disembark	
		fry.disembark.acquireUninterruptibly();	
		System.out.println("Auto " + id_auto + " disembarks from ferry at port " + port);
		fry.reduceLoad();   // Reduce load
		if(fry.getLoad() == 0){
			if(fry.getPort() == 0){
				for(int i = fry.getLoadAtOnce(); i > 0; i--){
					fry.carringPort0.release();		
				}
			}else{
				for(int i = fry.getLoadAtOnce(); i > 0; i--){
					fry.carringPort1.release();		
				}
			}
		}

		// Terminate
		if(isInterrupted()) break;
	   }
	   System.out.println("Auto "+id_auto+" terminated");
	}
 
}

class Ambulance extends Thread { // the Class for the Ambulance thread

	private int port;
	private Ferry fry;

	public Ambulance(int prt, Ferry ferry)
	{
		this.port = prt;
		this.fry = ferry;
	}

	public void run() 
        {
	   while (true) 
           {
		// Attente
		try {sleep((int) (1000*Math.random()));} catch (Exception e) { break;}
		System.out.println("Ambulance arrives at port " + port);

		// Board
		if(port == 0){
			fry.carringPort0.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.carringPort0.release();
				 break;
			}				
		}else{
			fry.carringPort1.acquireUninterruptibly();
			if(isInterrupted()) { 
				fry.carringPort1.release();
				 break;
			}
		}
		fry.ambulance.acquireUninterruptibly();
		System.out.println("Ambulance boards the ferry at port " + port);
		fry.addLoad();  // increment the load
		fry.crossing.release(); 
 		
		// Arrive at the next port
		port = 1 - port ;   

		//Disembarkment	
		fry.disembark.acquireUninterruptibly();	
		System.out.println("Ambulance disembarks the ferry at port " + port);
		fry.reduceLoad();   // Reduce load
		if(fry.getLoad() == 0){
			if(fry.getPort() == 0){
				for(int i = fry.getLoadAtOnce(); i > 0; i--){
					fry.carringPort0.release();		
				}
			}else{
				for(int i = fry.getLoadAtOnce(); i > 0; i--){
					fry.carringPort1.release();		
				}
			}
		}
		fry.ambulance.release();

		// Terminate
		if(isInterrupted()) break;
	   }
	   System.out.println("Ambulance terminates.");
	}
 
}

class Ferry extends Thread // The ferry Class
{
	private int port=0;  // Start at port 0
	private int load=0;  // Load is zero
	private int numCrossings;  // number of crossings to execute
	public Semaphore carringPort0;
	public Semaphore carringPort1;
	public Semaphore disembark;
	public Semaphore crossing;
	public Semaphore ambulance;
	private int loadAtOnce = 0;
	// Semaphores

	public Ferry(int prt, int nbtours)
	{
		this.port = prt;
		numCrossings = nbtours;
		carringPort1 = new Semaphore(5);
		carringPort0 = new Semaphore(5);
		crossing = new Semaphore(0);
		disembark = new Semaphore(0);
		ambulance = new Semaphore(1);
		if(port == 0){
			for(int i = 0; i < 5; i++){
				carringPort1.acquireUninterruptibly();
			}
		}else{
			for(int i = 0; i < 5; i++){
				carringPort0.acquireUninterruptibly();
			}
		}
	}

	public void run() 
        {
	   int i;
	   System.out.println("Start at port " + port + " with a load of " + load + " vehicles");

	   // numCrossings crossings in our day
	   for(i=0 ; i < numCrossings ; i++)
           {
		// The crossing
		crossing.acquireUninterruptibly();
		System.out.println("Departure from port " + port + " with a load of " + load + " vehicles");
		System.out.println("Crossing " + i + " with a load of " + load + " vehicles");
		port = 1 - port;
		try {sleep((int) (100*Math.random()));} catch (Exception e) { }
		// Arrive at port
		System.out.println("Arrive at port " + port + " with a load of " + load + " vehicles");
		loadAtOnce = load;
		// Disembarkment et loading
		for(int j = loadAtOnce; j > 0; j--){
			disembark.release();
		}
	   }
	}

	// methodes to manipulate the load of the ferry
	public int getLoad()      { return(load); }
	public int getPort()	{return port;}
	public void addLoad()  { load = load + 1; }
	public void reduceLoad()  { load = load - 1 ;}
	public int getLoadAtOnce() { return loadAtOnce;}
}
